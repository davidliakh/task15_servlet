package epam.model;

public class Book {
    private String author;
    private String name;
    private int price;
    private String genre;
    private int id;

    public Book(String name, String genre) {
        this.name = name;
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getGenre() {
        return genre;
    }

    @Override
    public String toString() {
        return "BookStore{" +
                "author='" + author + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", genre='" + genre + '\'' +
                '}';
    }
}
