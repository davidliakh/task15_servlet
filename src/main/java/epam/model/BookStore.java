package epam.model;

import java.util.ArrayList;
import java.util.List;

public class BookStore {
    private String genre;
    private int id;
    private String name;
    private List<Book> books;
    private int count;

    public BookStore(String name) {
        this.name = name;
        id = count++;
        books = new ArrayList<Book>();
    }

    public List<Book> getBooks() {
        return books;
    }

    public String getGenre() {
        return genre;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void addBook(Book book) {
        books.add(book);
    }

    @Override
    public String toString() {
        return "BookStore{" +
                "genre='" + genre + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
