package epam.controller.servlets;

import epam.model.BookStore;
import epam.util.DBList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/BookStore/*")
public class BookStoreServlet extends HttpServlet {
    private static Logger logger = LogManager.getLogger(BookStoreServlet.class);

    @Override
    public void init() throws ServletException {
        logger.info("Servlet " + getClass().getSimpleName() + " started");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<h1>Welcome to BookStore Manager</h1>");
        out.println("<h3>List of Books</h3>");
        getFacultiesFromRequest(out);
        saveBookStoreForm(out);
        deleteBookStoreForm(out);
        out.println("<p> <a href='BookStore'>REFRESH</a> </p>");
        resp.setStatus(HttpServletResponse.SC_OK);
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        BookStore bookStore = new BookStore(name);
        DBList.addToStore(bookStore);
        logger.info("Book with name " + name + " created");
        resp.setStatus(HttpServletResponse.SC_CREATED);
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getPathInfo().substring(1);
        DBList.getBookStores().removeIf(f -> f.getId() == (Integer.parseInt(id)));
        logger.info("Book with id " + id + " deleted");
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    public void destroy() {
        logger.info("Servlet " + getClass().getSimpleName() + " destroyed");
    }

    private void getFacultiesFromRequest(PrintWriter out) {
        for (BookStore bookStore : DBList.getBookStores()) {
            out.println("<p><a href=" + "books/" + bookStore.getName().toLowerCase() + ">");
            out.println(bookStore.getId() + " " + bookStore.getName() + " book list");
            out.println("</a></p>");
        }
    }

    private void deleteBookStoreForm(PrintWriter out) {
        out.println("<form>\n"
                + "  <p><b>Delete BookStore</b></p>\n"
                + "  <p> Book id: <input type='text' name='bookStore_id'>\n"
                + "    <input type='button' onclick='remove(this.form.bookStore_id.value)' name='ok' "
                + "value='Delete BookStore'/>\n"
                + "  </p>\n"
                + "</form>");

        out.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch('bookStore/' + id, {method: 'DELETE'}); }\n"
                + "</script>");
    }

    private void saveBookStoreForm(PrintWriter out) {
        out.println("<form action = \"/BookStore\" method=\"post\">\n" +
                "    <input required type=\"text\" name=\"name\" placeholder=\"BookStore Name\">\n" +
                "    <input type=\"submit\" value=\"Save BookStore\">\n" +
                "</form>");
    }
}

