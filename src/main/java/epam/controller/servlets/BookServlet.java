package epam.controller.servlets;

import epam.model.Book;
import epam.util.DBList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

@WebServlet("/books/*")
public class BookServlet extends HttpServlet {
    private static Logger logger = LogManager.getLogger(BookServlet.class);
    private String genre;
    @Override
    public void init()throws ServletException {
        logger.info("Servlet " + getClass().getSimpleName() + " started");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        genre = req.getPathInfo().substring(1);
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        out.println("<html><body>");
        out.println("<h3>List of Books</h3>");
        getBookFromRequest(out);
        saveBooksForm(out);
        goToBookStore(out);
        deleteBookForm(out);
        out.println("<p> <a href='../books/"+ genre + "'>REFRESH</a> </p>");
        out.println("</body></html>");
        res.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("Name");
        String author = req.getParameter("Author");
        Book book = new Book(name,author);
        Objects.requireNonNull(DBList.getBooksBy(genre)).get().addBook(book);
        logger.info("Book : " + name + " " + author );
        resp.setStatus(HttpServletResponse.SC_CREATED);
        doGet(req,resp);
    }
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getPathInfo().substring(1);
        Objects.requireNonNull(DBList.getBooksBy(genre)).get().getBooks()
                .removeIf( s -> s.getId() == (Integer.parseInt(id)));
        logger.info("Book with id " +  id  + " deleted");
        resp.setStatus(HttpServletResponse.SC_OK);
    }
    @Override
    public void destroy() {
        logger.info("Servlet " + getClass().getSimpleName() + " destroyed");
    }

    private void getBookFromRequest(PrintWriter out){
        for (Book book : Objects.requireNonNull(DBList.getBooksBy(genre)).get().getBooks()){
            out.println("<p>");
            out.println(book.getId() + " " +book.getName() + " " +
                    book.getAuthor() + " " + book.getPrice());
            out.println("</p>");
        }
    }
    private void saveBooksForm(PrintWriter out){
        out.println("<form action = \"/students/" + genre + "\"" + " method=\"post\">\n" +
                "    <input required type=\"text\" name=\"Name\" placeholder=\"First Name\">\n" +
                "    <input required type=\"text\" name=\"Author\" placeholder=\"Second Name\">\n" +
                "    <input required type=\"text\" name=\"price\" placeholder=\"Age\">\n" +
                "    <input type=\"submit\" value=\"Save Books\">\n" +
                "</form>");
    }
    private void deleteBookForm(PrintWriter out){
        out.println("<form>\n"
                + "  <p><b>Delete Book</b></p>\n"
                + "  <p> Book id: <input type='text' name='book_id'>\n"
                + "  <input type='button' onclick='remove(this.form.book_id.value)' name='ok' "
                + "value='Delete Book'/>\n"
                + "  </p>\n"
                + "</form>");

        out.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch(id, {method: 'DELETE'}); }\n"
                + "</script>");
    }
    private void goToBookStore(PrintWriter out){
        out.println("<p> <a href='../Books'>Go to books in Store</a> </p>");
    }

}
