package epam.util;

import epam.model.Book;
import epam.model.BookStore;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DBList {
    private static List<BookStore> bookStores;

    static {
        bookStores = new ArrayList<>();
        BookStore philosophy = new BookStore("Philosophy");
        BookStore scienceFiction = new BookStore("Science Fiction");
        BookStore cplusplus = new BookStore("C++ home study");
        philosophy.addBook(new Book("Edgar Po", "philosophy"));
        philosophy.addBook(new Book("Jackie Jan", "philosophy"));
        scienceFiction.addBook(new Book("Harry Potter", "Science Fiction"));
        scienceFiction.addBook(new Book("How to learn java in 1 day", "Science Fiction"));
        cplusplus.addBook(new Book("How to learn c++", "science"));
        cplusplus.addBook(new Book("How to learn Python", "science"));
        bookStores.add(philosophy);
        bookStores.add(scienceFiction);
        bookStores.add(cplusplus);
    }

    public static List<BookStore> getBookStores() {
        return bookStores;
    }

    public static void addToStore(BookStore bookStore) {
        bookStores.add(bookStore);
    }

    public static Optional<BookStore> getBooksBy(String genre) {
        for (BookStore bookStore : getBookStores()) {
            if (bookStore.getName().equalsIgnoreCase(genre)) {
                return Optional.of(bookStore);
            }
        }
        return Optional.empty();
    }
}
